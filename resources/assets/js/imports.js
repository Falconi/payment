let angular = require("angular");
let angular_cookies = require("angular-cookies");
let angular_route = require("angular-route");

let sweetalert = require("sweetalert");

let ConfigApp = require("./config/config_app");

let ServiceUsers = require("./services/service_users");
let ServiceBanks = require("./services/service_banks");

let FactoryBanks = require("./factories/factory_banks");
let FactoryUsers = require("./factories/factory_users");

let ControllerBanks = require("./controllers/controller_banks");
let ControllerConfirm = require("./controllers/controller_confirm");
let ControllerLogin = require("./controllers/controller_login");
let ControllerOrders = require("./controllers/controller_orders");

angular.module("module_app", [
    "ngRoute",
    "ngCookies"
]);

ConfigApp.routes.$inject = ["$routeProvider"];
angular.module("module_app").config(ConfigApp.routes);

ConfigApp.run.$inject = ["$rootScope", "$location", "FactoryBanks", "$cookies", "$window"];
angular.module("module_app").run(ConfigApp.run);

ServiceUsers.$inject = ["$http"];
angular.module("module_app").service("ServiceUsers", ServiceUsers);

ServiceBanks.$inject = ["$http"];
angular.module("module_app").service("ServiceBanks", ServiceBanks);

FactoryBanks.$inject = ["$log", "$q", "ServiceBanks"];
angular.module("module_app").factory("FactoryBanks", FactoryBanks);

FactoryUsers.$inject = ["$log", "$q", "ServiceUsers"];
angular.module("module_app").factory("FactoryUsers", FactoryUsers);

ControllerBanks.$inject = ["$scope", "FactoryBanks"];
angular.module("module_app").controller("ControllerBanks", ControllerBanks);

ControllerConfirm.$inject = ["$scope", "$location", "$cookies"];
angular.module("module_app").controller("ControllerConfirm", ControllerConfirm);

ControllerLogin.$inject = ["$cookies", "$scope", "$rootScope", "$route", "$location", "FactoryBanks", "FactoryUsers"];
angular.module("module_app").controller("ControllerLogin", ControllerLogin);

ControllerOrders.$inject = ["$scope"];
angular.module("module_app").controller("ControllerOrders", ControllerOrders);