module.exports.routes = function ($routeProvider) {
	
	// $locationProvider.html5Mode(true);
	
	let sufix = " | Payment";
	
	$routeProvider.when("/", {
		templateUrl: "html/pages/orders.html",
		controller: "ControllerOrders",
		title: "Pedidos" + sufix
	})
	
	.when("/checkout-banks", {
		templateUrl: "html/pages/banks.html",
		controller: "ControllerBanks",
		title: "Bancos" + sufix
	})
	
	.when("/checkout-login/:slug", {
		templateUrl: "html/pages/login.html",
		controller: "ControllerLogin",
		title: "Login" + sufix
	})
	
	.when("/checkout-confirm", {
		templateUrl: "html/pages/confirm.html",
		controller: "ControllerConfirm",
		title: "Confirmação" + sufix
	})
	
	$routeProvider.otherwise({redirectTo: "/"});	
}

module.exports.run = function($rootScope, $location, FactoryBanks, $cookies, $window){

	$rootScope.$on( "$routeChangeStart", function(event, next, current) {
		/* 	Aplicando o title da página e armazenando a página atual / anterior */
		document.querySelector("html head title").innerHTML = next.$$route.title;
		$rootScope.pageCurrent = next.$$route.originalPath;
		$rootScope.pagePrev = () => {
			return $window.history.back();
		}

		// Limpando o Cookie caso não esteja na tela de payment
		if(next.$$route.originalPath != "/checkout-confirm"){
			$cookies.remove("user");
		}

		// Rotas validações
		switch(next.$$route.originalPath){
			
			/* Validando o banco selecionado */
			case "/checkout-login/:slug":
				FactoryBanks.getList().then(response => {
					if(!response.filter(response => response.slug == next.params.slug)[0]){
						$location.path("/");
					}
				});
				break;
			
			/* Validando Tela de Confirmação do payment */
			case "/checkout-confirm":
				if(!$cookies.get("user")){
					$location.path("/");
				}
				break;

		}
	});		
}