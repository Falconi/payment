module.exports = function($log, $q, ServiceUsers){

    let USER = function(id, name, banks){
        this.id = id;
        this.name = name;
        this.banks = banks;
    };

    return{
		getList: () =>{
			let callback = $q.defer();
			ServiceUsers.GET_list().then(response => {
				if(response.data){
                    let users = new Array();
                    response.data.forEach(element => {
                        users.push(new USER(
                            element.id,
                            element.name,
                            element.banks
                        ));
                    });
					callback.resolve(users);
					$log.info("ServiceUsers: OK!");
				}else{
					callback.reject(false);
					$log.warn("ServiceUsers: Não há itens cadastrados");
				}
			},
			() => {
				callback.reject(false);
				$log.error("ServiceUsers: Não foi possível acessar as informações");
			});
			return callback.promise;
		},
		
		getItem: idUser => {
			let callback = $q.defer();
			ServiceUsers.GET_list().then(response => {
				if(response.data){
                    let result = response.data.filter(element => idUser == element.id)[0];
                    let user;
                    if(result){
                        user = new USER(
                            result.id,
                            result.name,
                            result.banks
                        );
                    }
					callback.resolve(user);
					$log.info("ServiceUser: OK!");
				}else{
					callback.reject(false);
					$log.warn("ServiceUser: Não há itens cadastrados");
				}
			},
			() => {
				callback.reject(false);
				$log.error("ServiceUser: Não foi possível acessar as informações");
			});
			return callback.promise;
        },
        
        getUserByLogin: (username, password, idBank) => {
			let callback = $q.defer();
			ServiceUsers.GET_list().then(response => {
				if(response.data){                    
                    let user;
                    response.data.forEach(element => {
                        let bankValidate = element.banks.filter(bank => {
                            return bank.id == idBank && bank.username == username && bank.password == password;
                        })[0];
                        if(bankValidate){
                            user = new USER(
                                element.id,
                                element.name,
                                element.banks
                            );
                            return false;
                        }
                    });
					callback.resolve(user);
					$log.info("ServiceUser: OK!");
				}else{
					callback.reject(false);
					$log.warn("ServiceUser: Não há itens cadastrados");
				}
			},
			() => {
				callback.reject(false);
				$log.error("ServiceUser: Não foi possível acessar as informações");
			});
			return callback.promise;
        }
    }
}