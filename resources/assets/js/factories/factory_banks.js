module.exports = function($log, $q, ServiceBanks){
	return{
		getList: () =>{
			let callback = $q.defer();
			ServiceBanks.GET_list().then(response => {
				if(response.data){
					callback.resolve(response.data);
					$log.info("ServiceBanks: OK!");
				}else{
					callback.reject(false);
					$log.warn("ServiceBanks: Não há itens cadastrados");
				}
			},
			() => {
				callback.reject(false);
				$log.error("ServiceBanks: Não foi possível acessar as informações");
			});
			return callback.promise;
		},
		
		getItem: bankSlug => {
			let callback = $q.defer();
			ServiceBanks.GET_list().then(response => {
				if(response.data){
					callback.resolve(response.data.filter(element => bankSlug == element.slug)[0]);
					$log.info("ServiceBanks: OK!");
				}else{
					callback.reject(false);
					$log.warn("ServiceBanks: Não há itens cadastrados");
				}
			},
			() => {
				callback.reject(false);
				$log.error("ServiceBanks: Não foi possível acessar as informações");
			});
			return callback.promise;
		}
	};
}