module.exports = function($scope, $location, $cookies){

    $scope.username = JSON.parse($cookies.get("user")).name;

    $scope.paymentConfirm = (event, validate) => {
        if(validate){
            swal({
                title: 'Pagamento efetuado com sucesso!',
                text: 'Embreve o banco entrará em contato...',
                icon: '/assets/img/icon-alert-success.svg',
                className: 'success',
                closeOnClickOutside: false,
                closeOnEsc: false,
                buttons: {
                    confirm: 'Ok' 
                } 
            }).then(value => {
                if(value){
                    $scope.$apply( () => {
                        $cookies.remove("user");
                        $location.path("/");
                    })
                }
            });
        }
    }
    
    $scope.buttonDeny = () => {
        swal({
            title: 'Cancelar',
            text: 'Tem certeza que deseja cancelar o pagamento?',
            icon: '/assets/img/icon-alert-warning.svg',
            className: 'confirm',
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: {
                cancel: 'Não',
                confirm: 'Sim' 
            } 
            }).then(function(value){
                if(value){
                    swal({ 
                        title: 'Cancelado com sucesso!',
                        text: 'O pagamento foi cancelado',
                        icon: '/assets/img/icon-alert-success.svg',
                        className: 'success',
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: {
                            confirm: 'Ok' 
                        } 
                    }).then( () => {
                        $scope.$apply( () => {
                            $cookies.remove("user");
                            $location.path("/");
                        })
                    });
                }
            }, function(){
                swal({ 
                    title: 'Não foi possível cancelar!',
                    text: 'O pagamento continua em aberto...',
                    icon: '/assets/img/icon-alert-error.svg',
                    className: 'error'
                });
            });        
    }
}