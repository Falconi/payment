module.exports = function($cookies, $scope, $rootScope, $route, $location, FactoryBanks, FactoryUsers){


    FactoryBanks.getList().then(response => {
        $scope.bank = response.filter(response => response.slug == $route.current.params.slug)[0];
    });
    
    $scope.login = (event, validate) => {
        if(validate){
            FactoryUsers.getUserByLogin($scope.email, $scope.password, $scope.bank.id)
            .then(response => {
                if(response){
                    $cookies.put("user", JSON.stringify(response));
                    $location.path("/checkout-confirm");
                }else{
                    swal({ 
                        title: 'Login Inválido',
                        text: 'Verifique se o e-mail e a senha estão corretos...',
                        icon: '/assets/img/icon-alert-error.svg',
                        className: 'error',
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: {
                            confirm: 'Ok' 
                        } 
                    }).then(value => {
                        $scope.$apply( () => {
                            event.target.email.focus();
                            $scope.email = "";
                            $scope.password = "";
                        });
                    });
                }
            });
        }
    }    
}