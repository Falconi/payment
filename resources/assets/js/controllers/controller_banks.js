module.exports = function($scope, FactoryBanks){
    
    FactoryBanks.getList().then(response => {
        $scope.banks = response;
    });
    
}