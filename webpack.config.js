const webpack = require('webpack');
const path = require('path');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname + "/resources/assets/"),
    entry: ['./js/imports.js', './scss/imports.scss'],
    output: {
        path: path.resolve(__dirname + "/public/assets/"),
        filename: "js/frontend.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                        cacheDirectory: true
                    }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader']
                })
            }            
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/frontend.css'),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            options: {
                postcss: [autoprefixer]       
            }
        }),
        new SVGSpritemapPlugin({
            src: path.resolve(__dirname + '/resources/assets/img/sprites/**/*.svg'),
            filename: 'img/icons.svg',
            prefix: '',
            svgo:{
                plugins: [{
                    removeTitle: true,
                    minify: true,
                    removeXMLNS: true
                }]
            }
        }),
        new CopyWebpackPlugin([
            {
                // Se colocar barra no final ele copia o diretório
                from: path.resolve(__dirname + '/resources/assets/img/pictures'),
                to: path.resolve(__dirname + '/public/assets/img/')
            }
        ])
    ]    
}